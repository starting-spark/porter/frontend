#!/bin/sh

# This script installs the application dependencies

export CYPRESS_INSTALL_BINARY=0 # skip installing cypress binary to avoid
# installation errors
# cypress is also not needed because cypress runs in a separate container
# unfortunately, some of cypress other artifacts are required to maintain
# application parity across all of the environments
# remove this workaround until a proper installation can be achieved or
# when the cypress dependency can be remove from package.json completely
npm install --global npm@latest
npm ci # not sure why --no-audit doesn't work when building container

