#!/bin/bash

# This script lints the shell code

findings_found=0
trap '(( findings_found |= $? ))' ERR

shellcheck -- *.sh
for file in scripts/**/*.sh; do
  shellcheck "$file"
done

exit $findings_found

