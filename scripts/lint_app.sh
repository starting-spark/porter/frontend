#!/bin/sh

# This script lints the application

SRC_DIR=$(cd "$(dirname "$0")" || exit; pwd -P)

"$SRC_DIR"/lint_app/lint_shell.sh
"$SRC_DIR"/lint_app/lint_javascript.sh

