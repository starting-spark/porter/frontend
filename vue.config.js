module.exports = {
  devServer: {
    public: process.env["DEV_SERVER_PUBLIC"],
  },
  outputDir: "artifacts/dist/",
};
