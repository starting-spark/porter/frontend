import { shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import Create from "@/components/greeting/create.vue";

describe("greeting/create.vue", () => {
  it("renders instructions", () => {
    const expectedInstructionRegex = /creat(.*)greeting/i;
    // should match 'create a greeting' or 'creating a greeting'
    const wrapper = shallowMount(Create);
    expect(wrapper.text()).toMatch(expectedInstructionRegex);
  });

  it("processes new greeting", () => {
    let actions = {
      addGreeting: jest.fn(),
    };
    let store = new Vuex.Store({
      modules: {
        greetings: {
          namespaced: true,
          actions,
        },
      },
    });
    const wrapper = shallowMount(Create, {
      global: {
        plugins: [store],
      },
    });

    const expectedMessage = "arbitrary text";
    wrapper.setData({
      greeting: {
        message: expectedMessage,
      },
    });

    wrapper.vm.processForm();
    expect(actions.addGreeting).toHaveBeenCalledWith(expect.anything(), {
      message: expectedMessage,
    });
  });
});
