import { shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import Update from "@/components/greeting/update.vue";

describe("greeting/update.vue", () => {
  it("updates greeting message", () => {
    let actions = {
      updateGreeting: jest.fn(),
    };
    let store = new Vuex.Store({
      modules: {
        greetings: {
          namespaced: true,
          actions,
        },
      },
    });
    const originalMessage = "arbitrary message";
    const wrapper = shallowMount(Update, {
      global: {
        plugins: [store],
      },
      props: {
        greeting: {
          attributes: {
            message: originalMessage,
          },
        },
      },
    });

    expect(wrapper.find("input").element.value).toEqual(originalMessage);
    expect(wrapper.vm["message"]).toEqual(originalMessage);

    const newMessage = "another arbitrary message";
    wrapper.find("[type=text]").setValue(newMessage);

    expect(actions.updateGreeting).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        newMessage: newMessage,
      })
    );
  });
});
