import { shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import Delete from "@/components/greeting/delete.vue";

describe("greeting/delete.vue", () => {
  it("renders instructions", () => {
    const expectedInstructionRegex = /delete(.*)greeting/i;
    // should match 'delete greeting' or 'delete a greeting'
    const wrapper = shallowMount(Delete);
    expect(wrapper.text()).toMatch(expectedInstructionRegex);
  });

  it("deletes greeting", async () => {
    let actions = {
      deleteGreeting: jest.fn(),
    };
    let store = new Vuex.Store({
      modules: {
        greetings: {
          namespaced: true,
          actions,
        },
      },
    });
    const wrapper = shallowMount(Delete, {
      global: {
        plugins: [store],
      },
    });

    const expectedMessage = "arbitrary text";
    await wrapper.setProps({
      greeting: {
        message: expectedMessage,
      },
    });

    wrapper.find("button").trigger("click");
    expect(actions.deleteGreeting).toHaveBeenCalledWith(expect.anything(), {
      message: expectedMessage,
    });
  });
});
