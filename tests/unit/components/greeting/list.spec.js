import { shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import List from "@/components/greeting/list.vue";

describe("greeting/list.vue", () => {
  it("renders all greetings", () => {
    const expectedMessage = "arbitrary message";
    const expectedGreeting = {
      attributes: {
        message: expectedMessage,
      },
    };
    let getters = {
      allGreetings: jest.fn(),
    };
    getters.allGreetings.mockReturnValue([expectedGreeting]);
    let actions = {
      getGreetings: jest.fn(),
    };
    let store = new Vuex.Store({
      modules: {
        greetings: {
          namespaced: true,
          getters,
          actions,
        },
      },
    });
    const wrapper = shallowMount(List, {
      global: {
        plugins: [store],
      },
    });

    expect(wrapper.vm.greetings).toEqual(
      expect.arrayContaining([expectedGreeting])
    );
    const firstListElementCssSelector = "li:first-child";
    expect(wrapper.find(firstListElementCssSelector).text()).toEqual(
      expectedMessage
    );
  });
});
