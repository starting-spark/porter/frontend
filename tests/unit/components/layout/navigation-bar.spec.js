import NavigationBar from "@/components/layout/navigation-bar.vue";
import router from "@/router";
import { shallowMount } from "@vue/test-utils";

describe("layout/navigation-bar.vue", () => {
  it("renders links", async () => {
    router.push("/");
    await router.isReady();
    const wrapper = shallowMount(NavigationBar, {
      global: {
        plugins: [router],
      },
    });

    const linkCssSelector = "router-link-stub";
    const expectedNumberOfLinks = 4;
    expect(wrapper.findAll(linkCssSelector)).toHaveLength(
      expectedNumberOfLinks
    );
  });
});
