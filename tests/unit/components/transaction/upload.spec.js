import { shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import Upload from "@/components/transaction/upload.vue";

describe("transaction/upload.vue", () => {
  it("renders instructions", () => {
    const expectedInstructionRegex = /upload transaction(s?)/i;
    // should match 'upload transaction' or 'upload transactions'

    let getters = {
      uploadStatus: jest.fn(),
    };
    let store = new Vuex.Store({
      modules: {
        transactions: {
          namespaced: true,
          getters,
        },
      },
    });
    const wrapper = shallowMount(Upload, {
      global: {
        plugins: [store],
      },
    });

    expect(wrapper.text()).toMatch(expectedInstructionRegex);
  });

  it("selects a file", () => {
    let getters = {
      uploadStatus: jest.fn(),
    };
    let store = new Vuex.Store({
      modules: {
        transactions: {
          namespaced: true,
          getters,
        },
      },
    });
    const wrapper = shallowMount(Upload, {
      global: {
        plugins: [store],
      },
    });

    wrapper.get("[name=file]").trigger("change");
    expect(wrapper.vm.$data).toBeTruthy(); // not sure what to test; using test
    //                                        to fulfill test coverage
  });

  it("uploads transactions file", () => {
    let getters = {
      uploadStatus: jest.fn(),
    };
    let actions = {
      uploadTransactions: jest.fn(),
    };
    let store = new Vuex.Store({
      modules: {
        transactions: {
          namespaced: true,
          getters,
          actions,
        },
      },
    });
    const wrapper = shallowMount(Upload, {
      global: {
        plugins: [store],
      },
    });

    const file = new File([""], {
      type: "text/csv",
    });
    wrapper.setData({
      file: [file],
    });

    const firstElementFound = 0;
    const uploadInstruction = /upload/i;
    wrapper
      .findAll("button")
      .filter((node) => node.text().match(uploadInstruction))
      .at(firstElementFound)
      .trigger("click");
  });
});
