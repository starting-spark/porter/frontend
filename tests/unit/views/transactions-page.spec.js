import { shallowMount } from "@vue/test-utils";
import router from "@/router/index";
import Transactions from "@/views/transactions-page.vue";

describe("views/transactions.vue", () => {
  it("renders information", () => {
    const expectedInformationRegex = /transactions/i;
    const wrapper = shallowMount(Transactions);
    expect(wrapper.text()).toMatch(expectedInformationRegex);
  });
  it("routes to transactions page", async () => {
    const expectedPath = "/transactions/";
    const expectedRouteName = "transactions";
    await router.push(expectedPath); // will log a warning if route fails
    expect(router.resolve(expectedPath)["name"]).toEqual(expectedRouteName);
    expect(router.hasRoute(expectedRouteName)).toBeTruthy();
  });
});
