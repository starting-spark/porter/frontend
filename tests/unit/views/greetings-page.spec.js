import { shallowMount } from "@vue/test-utils";
import router from "@/router/index";
import Greetings from "@/views/greetings-page.vue";

describe("views/greetings.vue", () => {
  it("renders information", () => {
    const expectedInformationRegex = /greeting/i;
    const wrapper = shallowMount(Greetings);
    expect(wrapper.text()).toMatch(expectedInformationRegex);
  });
  it("routes to greetings page", async () => {
    const expectedPath = "/greetings/";
    const expectedRouteName = "greetings";
    await router.push(expectedPath); // will log a warning if route fails
    expect(router.resolve(expectedPath)["name"]).toEqual(expectedRouteName);
    expect(router.hasRoute(expectedRouteName)).toBeTruthy();
  });
});
