import mutations from "@/store/modules/transactions/mutations";
import uploadStatusStates from "@/store/modules/transactions/upload_status_states";

describe("store/modules/transactions/mutations", () => {
  let state;
  beforeEach(() => {
    state = {
      uploadStatus: uploadStatusStates["READY"],
    };
  });

  it("updates status to started", () => {
    expect(state.uploadStatus).not.toEqual(uploadStatusStates["STARTED"]);
    mutations.changeUploadStatusToUploading(state);
    expect(state.uploadStatus).toEqual(uploadStatusStates["STARTED"]);
  });

  it("updates status to succeeded", () => {
    expect(state.uploadStatus).not.toEqual(uploadStatusStates["SUCCEEDED"]);
    mutations.changeUploadStatusToSucceeded(state);
    expect(state.uploadStatus).toEqual(uploadStatusStates["SUCCEEDED"]);
  });

  it("updates status to failed", () => {
    expect(state.uploadStatus).not.toEqual(uploadStatusStates["FAILED"]);
    mutations.changeUploadStatusToFailed(state);
    expect(state.uploadStatus).toEqual(uploadStatusStates["FAILED"]);
  });
});
