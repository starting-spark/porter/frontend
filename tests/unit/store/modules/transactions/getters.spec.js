import getters from "@/store/modules/transactions/getters";

describe("store/modules/transactions/getters", () => {
  it("gets uploadStatus", () => {
    const arbitraryStatus = "arbitrary status";
    const state = {
      uploadStatus: arbitraryStatus,
    };

    const results = getters.uploadStatus(state);

    expect(results).toEqual(arbitraryStatus);
  });
});
