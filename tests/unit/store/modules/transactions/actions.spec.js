import $axios from "@/services/backend-service";
import actions from "@/store/modules/transactions/actions";

describe("store/modules/transactions/actions", () => {
  let state;
  let postSpy;
  beforeEach(() => {
    state = {
      commit: jest.fn(),
    };
    postSpy = jest.spyOn($axios, "post");
  });

  it("starts uploading transactions", async () => {
    postSpy.mockImplementation(() => {
      return Promise.resolve();
    });

    await actions.uploadTransactions(state, { file: "arbitrary filename" });
    $axios.interceptors.request.handlers[0].fulfilled();

    expect(state.commit).toHaveBeenCalledWith("changeUploadStatusToUploading");
  });

  it("succeeds uploading transactions", async () => {
    postSpy.mockImplementation(() => {
      return Promise.resolve();
    });

    await actions.uploadTransactions(state, { file: "arbitrary filename" });

    expect(state.commit).toHaveBeenCalledWith("changeUploadStatusToSucceeded");
  });

  it("fails uploading transactions", async () => {
    postSpy.mockImplementation(() => {
      return Promise.reject({
        response: true,
      });
    });

    await actions.uploadTransactions(state, { file: "arbitrary filename" });

    expect(state.commit).toHaveBeenCalledWith("changeUploadStatusToFailed");
  });
});
