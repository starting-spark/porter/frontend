import router from "@/router/index";

describe("Router", () => {
  it("routes to root", async () => {
    const expectedPath = "/";
    const expectedRouteName = "home";
    await router.push(expectedPath); // will log a warning if route fails
    expect(router.resolve(expectedPath)["name"]).toEqual(expectedRouteName);
    expect(router.hasRoute(expectedRouteName)).toBeTruthy();
  });
  it("routes to about", async () => {
    const expectedPath = "/about/";
    const expectedRouteName = "about";
    await router.push(expectedPath); // will log a warning if route fails
    expect(router.resolve(expectedPath)["name"]).toEqual(expectedRouteName);
    expect(router.hasRoute(expectedRouteName)).toBeTruthy();
  });
  // each view's route is tested within the view's unit test
});
