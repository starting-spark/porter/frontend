describe("Greetings", () => {
  before(() => {
    cy.visit("/");
    cy.get("nav")
      .contains(/greetings/i)
      .click();
    // hack to ensure at least one greeting exists
    // because cypress does not allow for conditional checking
    // https://docs.cypress.io/guides/core-concepts/conditional-testing.html
    cy.get("form").find("[name=greeting]").type("arbitrary message");
    cy.get("form").find("[type=submit]").click();
    cy.get("form").find("[name=greeting]").clear();
    // hack to ensure at least one greeting exists:end
    cy.get("button:contains('Delete Greeting')").each(($el) => {
      cy.wrap($el).click();
    });
  });

  it("Create, update, and delete greetings", () => {
    const newMessage = "new message";
    const anotherNewMessage = "another new message";

    cy.get("form").find("[name=greeting]").type(newMessage);
    cy.get("form").find("[type=submit]").click();

    cy.get(`li:contains(${newMessage})`).should("exist");
    cy.get(`li:contains(${anotherNewMessage})`).should("not.exist");

    cy.get("form").find("[name=greeting]").clear().type(anotherNewMessage);
    cy.get("form").find("[type=submit]").click();

    cy.get(`li:contains(${newMessage})`).should("exist");
    cy.get(`li:contains(${anotherNewMessage})`).should("exist");

    const updatedMessage = "updated message";
    cy.get(`li:contains(${anotherNewMessage})`)
      .find("input[type=text]")
      .invoke("val", updatedMessage)
      .trigger("input");

    cy.get(`li:contains(${newMessage})`).should("exist");
    cy.get(`li:contains(${anotherNewMessage})`).should("not.exist");
    cy.get(`li:contains(${updatedMessage})`).should("exist");

    cy.get(`li:contains(${newMessage})`)
      .find("button:contains('Delete Greeting')")
      .click();
    cy.get(`li:contains(${updatedMessage})`)
      .find("button:contains('Delete Greeting')")
      .click();

    cy.get("ul").children().should("not.exist");
  });
});
