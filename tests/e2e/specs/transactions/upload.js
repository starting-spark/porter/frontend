describe("Uploading", () => {
  before(() => {
    cy.visit("/");
    cy.get("nav")
      .contains(/transactions/i)
      .click();
  });

  it("Upload transactions", () => {
    const defaultMessage = "ready";
    const inProgressMessage = "uploading";
    const failedMessage = "failed";

    cy.get(`*:contains(${defaultMessage})`).should("exist");
    cy.get(`*:contains(${inProgressMessage})`).should("not.exist");
    cy.get(`*:contains(${failedMessage})`).should("not.exist");

    cy.get("form").find("[name=file]").click();
    // when Cypress 9.3 or 10.0 is released, replace with .selectFile:
    // cy.get('[name=file]').selectFile('cypress/fixtures/myfixture.json');
    // https://github.com/cypress-io/cypress/issues/170
    cy.get("form")
      .find("button")
      .contains("Upload", { matchCase: false })
      .click();

    cy.get(`*:contains(${defaultMessage})`).should("not.exist");
    cy.get(`*:contains(${inProgressMessage})`).should("exist"); // cypress is
    // able to pick up this message even when it is only quickly flashed
    cy.get(`*:contains(${failedMessage})`).should("exist");

    // test for completion when e2e test is able to send file
  });
});
