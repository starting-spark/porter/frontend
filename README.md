# Porter:Frontend
The web frontend for the Porter application.

[![pipeline status](https://gitlab.com/starting-spark/porter/frontend/badges/master/pipeline.svg)](https://gitlab.com/starting-spark/porter/frontend/-/commits/master)
[![coverage report](https://gitlab.com/starting-spark/porter/frontend/badges/master/coverage.svg)](https://gitlab.com/starting-spark/porter/frontend/-/commits/master)

Prerequisites
-------------
1. install Docker
1. install Docker Compose
1. install Git
1. clone repository: `git clone --recursive https://gitlab.com/starting-spark/porter/frontend.git`

Getting Started
---------------
1. run bootstrap.sh: `./bootstrap.sh`
1. start service: `docker compose --file docker-compose.yml --file backend/docker-compose.yml --file docker-compose.override.yml up`
1. browse to: `localhost:8080`

Testing
-------
To unit test the application:

    frontend$ scripts/test_app.sh

To run a single describe group from unit tests:

    frontend$ npm run test:unit -- views/greetings

To automatically rerun unit tests upon file changes.

    frontend$ npm run exec -- nodemon --exec "vue-cli-service test:unit"
    or more practically
    frontend$ npm run exec -- nodemon --exec "clear; vue-cli-service test:unit views/greetings"

To end-to-end test the application:

    $ docker compose \
        --file docker-compose.yml \
        --file backend/docker-compose.yml \
        --file docker-compose.override.yml \
        --profile e2e \
        up \
        e2e

To run a single end-to-end test file:

    $ docker compose \
        --file docker-compose.yml \
        --file backend/docker-compose.yml \
        --file docker-compose.override.yml \
        run \
        e2e \
        --spec tests/e2e/specs/greetings.js

    or

    $ docker compose \
        --file docker-compose.yml \
        --file backend/docker-compose.yml \
        --file docker-compose.override.yml \
        run \
        --entrypoint bash \
        e2e
    e2e$ cypress run --spec tests/e2e/specs/greetings.js

To run a single test, append .only() to the test.

    it.only("renders instructions", () => {...});

Note that all test files are executed in parallel so .only() does not prevent
Jest from running all other test files. This means .only() will include a
single test within a test file. Be sure to name the tests uniquely, otherwise
the regex matching will still include tests with the same name.

To run end-to-end test suite on x86/Intel architecture:

    $ docker compose \
        --file docker-compose.yml \
        --file backend/docker-compose.yml \
        --file docker-compose.override.yml \
        --profile e2e \
        up \
        e2e-x86

Linting
-------
To lint the application:

    frontend$ scripts/lint_app.sh

Documenting
-----------
To document the application:

    frontend$ scripts/document_app.sh

To serve the application documentation:

    $ docker-compose \
        --file docker-compose.yml \
        --file backend/docker-compose.yml \
        --file docker-compose.override.yml \
        run --publish=5000:5000 frontend scripts/serve_documentation.sh

Production
----------
To run the production container:

    $ docker compose \
        --file docker-compose.yml \
        --file backend/docker-compose.yml \
        --file docker-compose.override.yml \
        --profile production \
        up \
        frontend-production

To shell into production container:

    $ docker-compose \
        --file docker-compose.yml \
        --file backend/docker-compose.yml \
        --file docker-compose.override.yml \
        run \
        --entrypoint "/bin/sh" \
        frontend-production

To build/re-deploy:

    $ docker-compose \
        --file docker-compose.yml \
        --file backend/docker-compose.yml \
        --file docker-compose.override.yml \
        run \
        --env VUE_APP_BACKEND_PORT=8001 \
        frontend \
        scripts/build_app.sh
    Reload your current page, ignoring cached content:
      for Mac Chrome, it's usually CMD+Shift+r

Notes
-----
See [Configuration Reference](https://cli.vuejs.org/config/).

