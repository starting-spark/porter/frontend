module.exports = {
  reporters: [
    "default",
    [
      "jest-junit",
      {
        outputDirectory: "artifacts/",
      },
    ],
  ],
  collectCoverage: true,
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
  coverageReporters: ["cobertura", "html", "text"],
  coverageDirectory: "artifacts/coverage/",
  preset: "@vue/cli-plugin-unit-jest",
  transform: {
    "^.+\\.vue$": "vue-jest",
  },
};
