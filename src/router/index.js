import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/home-page.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/about-page.vue"),
  },
  {
    path: "/greetings",
    name: "greetings",
    component: () =>
      import(/* webpackChunkName: "greetings" */ "../views/greetings-page.vue"),
  },
  {
    path: "/transactions",
    name: "transactions",
    component: () =>
      import(
        /* webpackChunkName: "transactions" */ "../views/transactions-page.vue"
      ),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
