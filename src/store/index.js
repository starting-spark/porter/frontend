import { createStore } from "vuex";
import Greetings from "./modules/greetings/";
import Transactions from "./modules/transactions/";

export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    greetings: Greetings,
    transactions: Transactions,
  },
});
