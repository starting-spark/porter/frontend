export const getters = {
  uploadStatus: (state) => {
    return state.uploadStatus;
  },
};

export default getters;
