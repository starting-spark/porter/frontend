const UploadStatusStates = {
  READY: "ready",
  STARTED: "uploading",
  SUCCEEDED: "succeeded",
  FAILED: "failed",
};

export default UploadStatusStates;
