import uploadStatusStates from "./upload_status_states";

export const mutations = {
  changeUploadStatusToUploading(state) {
    state.uploadStatus = uploadStatusStates["STARTED"];
  },
  changeUploadStatusToSucceeded(state) {
    state.uploadStatus = uploadStatusStates["SUCCEEDED"];
  },
  changeUploadStatusToFailed(state) {
    state.uploadStatus = uploadStatusStates["FAILED"];
  },
};

export default mutations;
