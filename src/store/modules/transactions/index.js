import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";
import uploadStatusStates from "./upload_status_states";

const Transactions = {
  namespaced: true,
  state: {
    uploadStatus: uploadStatusStates["READY"],
  },
  actions,
  getters,
  mutations,
};

export default Transactions;
