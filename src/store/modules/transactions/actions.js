import $axios from "@/services/backend-service";

const RESOURCE_NAME = "transaction";
const RESOURCE_PATH = `${RESOURCE_NAME}s`;

export const actions = {
  uploadTransactions(state, payload) {
    let formData = new FormData();
    formData.append("account_id", 1); // change to get dynamically when ready
    formData.append("file", payload["file"]);

    $axios.interceptors.request.use(function (config) {
      state.commit("changeUploadStatusToUploading");
      return config;
    });

    return $axios
      .post(`${RESOURCE_PATH}/batch_upload/`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(() => {
        state.commit("changeUploadStatusToSucceeded");
      })
      .catch(function () {
        state.commit("changeUploadStatusToFailed");
      });
  },
};

export default actions;
