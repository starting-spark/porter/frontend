import $axios from "@/services/backend-service.js";

const RESOURCE_NAME = "greeting";
const RESOURCE_PATH = `${RESOURCE_NAME}s`;

export const actions = {
  addGreeting(state, payload) {
    return $axios
      .post(`${RESOURCE_PATH}/`, {
        data: {
          type: RESOURCE_NAME,
          attributes: {
            message: payload["message"],
          },
        },
      })
      .then((response) => {
        state.commit("addGreeting", response["data"]["data"]);
      });
  },
  getGreetings(state) {
    return $axios
      .get(`${RESOURCE_PATH}/`)
      .then((response) => {
        const greetings = response["data"]["data"];
        state.commit("instateGreetings", greetings);
      })
      .catch(function (error) {
        console.log(error);
      });
  },
  updateGreeting(state, payload) {
    const id = payload["updatedGreeting"]["id"];
    return $axios
      .put(`${RESOURCE_PATH}/${id}/`, {
        data: {
          type: RESOURCE_NAME,
          id: id,
          attributes: {
            message: payload["newMessage"],
          },
        },
      })
      .then((response) => {
        const newGreeting = response["data"]["data"];
        state.commit("updateGreeting", {
          oldGreeting: state.getters.findGreeting(newGreeting["id"]),
          newGreeting: newGreeting,
        });
      });
  },
  deleteGreeting(state, deletedGreeting) {
    const id = deletedGreeting["id"];
    return $axios.delete(`${RESOURCE_PATH}/${id}/`).then(() => {
      state.commit("deleteGreeting", deletedGreeting);
    });
  },
};

export default actions;
