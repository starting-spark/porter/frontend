import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const Greetings = {
  namespaced: true,
  state: {
    greetings: [],
  },
  getters,
  mutations,
  actions,
};

export default Greetings;
