export const getters = {
  allGreetings: (state) => {
    return state.greetings;
  },
  findGreeting: (state) => (id) => {
    return state.greetings.find((message) => message.id === id);
  },
};

export default getters;
