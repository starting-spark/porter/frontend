export const mutations = {
  addGreeting(state, greeting) {
    state.greetings.push(greeting);
  },
  instateGreetings(state, greetings) {
    state.greetings = greetings;
  },
  updateGreeting(state, payload) {
    const newGreeting = payload["newGreeting"];
    const oldGreeting = payload["oldGreeting"];
    for (let key in newGreeting["attributes"]) {
      oldGreeting["attributes"][key] = newGreeting["attributes"][key];
    }
  },
  deleteGreeting(state, deletedGreeting) {
    const index = state.greetings.findIndex(
      (greeting) => greeting["id"] === deletedGreeting["id"]
    );
    const noGreetingWithId = -1;
    if (index != noGreetingWithId) state.greetings.splice(index, 1);
  },
};

export default mutations;
