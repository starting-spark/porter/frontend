import axios from "axios";

const API_BASE_URL =
  `${process.env["VUE_APP_BACKEND_SCHEME"]}` +
  `://` +
  `${process.env["VUE_APP_BACKEND_HOST"]}` +
  `:` +
  `${process.env["VUE_APP_BACKEND_PORT"]}` +
  `/` +
  `${process.env["VUE_APP_BACKEND_PATH_PREFIX"]}`;

const $axios = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    "Content-Type": "application/vnd.api+json",
  },
});

export default $axios;
