FROM registry.access.redhat.com/ubi8/nodejs-16 as development
ARG APP_DIR=/usr/src/app/
WORKDIR ${APP_DIR}/

USER root
RUN rpm --install \
  https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum --nobest --assumeyes update && yum --allowerasing --assumeyes install \
    ShellCheck \
  && yum clean all

COPY package*.json ${APP_DIR}/
COPY scripts/install_dependencies.sh ${APP_DIR}/scripts/install_dependencies.sh
RUN scripts/install_dependencies.sh
USER default

COPY . ${APP_DIR}

USER root
RUN mkdir -p ${APP_DIR}/artifacts/dist/ \
  && chown default ${APP_DIR}/artifacts/dist/ # remove workaround if there's
  # another way to allow container to build with properly and avoid permission
  # issues on named volume:
  # https://github.com/docker/compose/issues/3270#issuecomment-364254874
USER default


FROM development as builder
ARG VUE_APP_BACKEND_SCHEME=http
ARG VUE_APP_BACKEND_HOST=backend
ARG VUE_APP_BACKEND_PORT=8000
ARG VUE_APP_BACKEND_PATH_PREFIX=

RUN scripts/build_app.sh


FROM nginx:1.19.4-alpine as production
ARG APP_DIR=/usr/src/app/
ARG WEB_DIR=/usr/share/nginx/html/
ARG WEB_CONF_DIR=/etc/nginx/
WORKDIR ${WEB_DIR}/

RUN apk update \
  && rm -rf /var/cache/apk/*

COPY production/nginx.conf ${WEB_CONF_DIR}/.
COPY --from=builder ${APP_DIR}/artifacts/dist/ ${WEB_DIR}/

